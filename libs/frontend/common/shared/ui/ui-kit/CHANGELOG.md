# [1.1.0](https://gitlab.com/DumplingRu/nx-tutorial/compare/ui-kit-v1.0.1...ui-kit-v1.1.0) (2024-02-12)


### Features

* Fix ui ([30cc1e7](https://gitlab.com/DumplingRu/nx-tutorial/commit/30cc1e7e7c2cdafe2c0101263817a4d3d1f3ee20))

## [1.0.1](https://gitlab.com/DumplingRu/nx-tutorial/compare/ui-kit-v1.0.0...ui-kit-v1.0.1) (2024-02-08)


### Bug Fixes

* Ui-kit ([a9ccbb2](https://gitlab.com/DumplingRu/nx-tutorial/commit/a9ccbb2b8136cb4f2ba74ba3f2ee3616387fa1c4))

# 1.0.0 (2024-02-07)


### Bug Fixes

* Add yarn-lock ([58b1a4f](https://gitlab.com/DumplingRu/nx-tutorial/commit/58b1a4f76c1e93d4087aec606e5bc65031931217))
* commitlint ([a940cdb](https://gitlab.com/DumplingRu/nx-tutorial/commit/a940cdbb0e962492848681f0daf94e1739c5f5c3))
* fix version ([afc997e](https://gitlab.com/DumplingRu/nx-tutorial/commit/afc997ef97718051bb2f97e8b2a8e23d4b98dd02))


### Features

* Change ui-kit ([7146b13](https://gitlab.com/DumplingRu/nx-tutorial/commit/7146b13c9d5fa610fe4ec9ab2a1a28bd8df7fff4))
