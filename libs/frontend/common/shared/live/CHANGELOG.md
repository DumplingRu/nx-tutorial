## [1.0.2](https://gitlab.com/DumplingRu/nx-tutorial/compare/live-v1.0.1...live-v1.0.2) (2024-02-12)


### Bug Fixes

* Live ([d2e1444](https://gitlab.com/DumplingRu/nx-tutorial/commit/d2e14446eea45de6ab756eaed3900801c6581e84))
* Live ([41a5139](https://gitlab.com/DumplingRu/nx-tutorial/commit/41a5139b2fc6d2f63fdd87c10a061c3355e239ae))

## [1.0.1](https://gitlab.com/DumplingRu/nx-tutorial/compare/live-v1.0.0...live-v1.0.1) (2024-02-07)


### Bug Fixes

* Live ([5acd248](https://gitlab.com/DumplingRu/nx-tutorial/commit/5acd2486d5b943a10c704dadad9840d879a9174a))

# 1.0.0 (2024-02-07)


### Bug Fixes

* Add yarn-lock ([58b1a4f](https://gitlab.com/DumplingRu/nx-tutorial/commit/58b1a4f76c1e93d4087aec606e5bc65031931217))
* commitlint ([a940cdb](https://gitlab.com/DumplingRu/nx-tutorial/commit/a940cdbb0e962492848681f0daf94e1739c5f5c3))
* Fix depends ([b956aaf](https://gitlab.com/DumplingRu/nx-tutorial/commit/b956aaf2790579ab0bf49d6020996acb53fcc1b4))
* fix version ([afc997e](https://gitlab.com/DumplingRu/nx-tutorial/commit/afc997ef97718051bb2f97e8b2a8e23d4b98dd02))
